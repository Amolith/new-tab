This is the source code for my ["New tab"](https://nixnet.xyz/new-tab/) page, forked from [ceda_ei's repo](https://git.webionite.com/ceda_ei/Portfolio).

![](fullscreen.png)
![](desktop.png)
